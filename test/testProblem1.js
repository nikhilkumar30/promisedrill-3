const getBoardsInfo = require("../problem1");
const boardData = require("../boards_1.json");

let boardID = "mcu453ed";

getBoardsInfo(boardID, boardData)
  .then((data) => {
    console.log("Board Information:", data);
  })
  .catch((err) => {
    console.error(err);
  });

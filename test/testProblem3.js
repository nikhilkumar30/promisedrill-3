const cardsInfo = require("../problem3");
const cardData = require("../cards_1.json");

const listID = "qwsa221";

cardsInfo(listID, cardData)
  .then((data) => {
    console.log(`Card list of ${listID}:`, data);
  })
  .catch((err) => {
    console.error(err);
  });

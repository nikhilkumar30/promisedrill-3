const getListInfo = require("../problem2");
const listData = require("../lists_1.json");

let boardID = "mcu453ed";

getListInfo(boardID, listData)
  .then((data) => {
    console.log("List Information:", data);
  })
  .catch((err) => {
    console.error(err);
  });

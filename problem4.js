/* 
	Problem 4: Write a function that will use the previously written functions to get the following information. 

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously

*/

const getBoardsInfo = require("./problem1");
const getListInfo = require("./problem2");
const cardsInfo = require("./problem3");

function getAllData(boardData, listData, cardData) {
  return getBoardsInfo("mcu453ed", boardData)
    .then((boardInformation) => {
      console.log("Board Information of Thanos boards:", boardInformation);
      return getListInfo("mcu453ed", listData);
    })
    .then((listInformation) => {
      console.log("List Information:", listInformation);
      return cardsInfo("qwsa221", cardData);
    })
    .then((cardList) => {
      console.log("Card list of qwsa221:", cardList);
    })
    .catch((error) => {
      console.error("Error:", error);
    });
}

module.exports = getAllData;

/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json.
*/

function cardsInfo(listID, cardData) {
  return new Promise((resolve, reject) => {
    const card = cardData[listID];

    if (card) {
      resolve(card);
    } else {
      reject("Error");
    }
  });
}

module.exports = cardsInfo;

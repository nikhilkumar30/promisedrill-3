/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json.
*/

function getBoardsInfo(boardID, boardData) {
    return new Promise((resolve, reject) => {
      const board = boardData.find((board) => board.id === boardID);
  
      if (board) {
        resolve(board);
      } else {
        reject("Error");
      }
    });
  }
  
  module.exports = getBoardsInfo;
  
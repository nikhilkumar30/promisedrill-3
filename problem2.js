/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json.
*/


function getListInfo(boardID, listData) {
  return new Promise((resolve, reject) => {
    const list = listData[boardID];

    if (list) {
      resolve(list);
    } else {
      reject("Error");
    }
  });
}

module.exports = getListInfo;

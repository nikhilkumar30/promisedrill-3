/* 
	Problem 5: Write a function that will use the previously written functions to get the following information. 

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/

const getBoardsInfo = require("./problem1");
const getListInfo = require("./problem2");
const cardsInfo = require("./problem3");

function getAllData(boardData, listData, cardData) {
  const boardID = "mcu453ed";

  return getBoardsInfo(boardID, boardData)
    .then((boardInformation) => {
      console.log("Board Information of Thanos boards:", boardInformation);

      return getListInfo(boardID, listData);
    })
    .then((listInformation) => {
      console.log("List Information:", listInformation);

      const listID = "qwsa221";
      const listID2 = "jwkh245";

      const cardPromise1 = cardsInfo(listID, cardData)
        .then((data) => console.log(`Card list of ${listID}:`, data))
        .catch((err) => console.error(`Error ${err}`));

      const cardPromise2 = cardsInfo(listID2, cardData)
        .then((data) => console.log(`Card list of ${listID2}:`, data))
        .catch((err) => console.error(`Error ${err}`));

      return Promise.all([cardPromise1, cardPromise2]);
    })
    .catch((error) => {
      console.error("Error:", error);
    });
}

module.exports = getAllData;

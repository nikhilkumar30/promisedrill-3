/*
Problem 6: Write a function that will use the previously written functions to get the following information.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

const getBoardsInfo = require("./problem1");
const getListInfo = require("./problem2");
const cardsInfo = require("./problem3");

function getAllData(boardData, listData, cardData) {
  const boardID = "mcu453ed";

  return getBoardsInfo(boardID, boardData)
    .then((boardInformation) => {
      console.log("Board Information of Thanos boards:", boardInformation);

      return getListInfo(boardID, listData);
    })
    .then((listInformation) => {
      console.log("List Information:", listInformation);

      const listIDs = listInformation.map((list) => list.id);
      console.log(listIDs);

      const cardPromises = listIDs.map((listID) => {
        return cardsInfo(listID, cardData)
          .then((data) => console.log(`Card list of ${listID}:`, data))
          .catch((err) =>
            console.error(`No card for this id ${listID} ${err}`)
          );
      });

      return Promise.all(cardPromises);
    })
    .catch((error) => {
      console.error("Error:", error);
    });
}

module.exports = getAllData;
